<?php /* Template Name: page-links */ ?>
<?php get_header(); ?> <!-- ouvrir header,php -->
<main>
  <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

  <article class="article-content" id="post-<?php the_ID(); ?>">
  <h1 class="page-title"><?php the_title(); ?></h1>

    <div class="article-body mb-2">
      <?php the_content(); ?>
    </div>
  </article>

  <div class="card head-primary">
    <h2 class="card-header">Liste de mes réseaux sociaux</h2>
    <table>
      <thead>
        <tr>
          <th class="text-info">Réseau social</th>
          <th class="text-right text-info">Lien</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['social']);
        if ($menu != null) {
          foreach ($menu as $menuElement) {
            ?>
              <tr>
                <th><svg class="icon" alt=""><use xlink:href="#icon-<?php echo $menuElement->title; ?>"></use></svg> <span class="text-maj"><?php echo $menuElement->title; ?></span></th>
                <td class="text-right"><a class="pretty-link" href="<?php echo $menuElement->url; ?>"><?php echo $menuElement->url; ?></td>
              </tr>
            <?php
          }
        }
        ?>
        <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['social-plus']);
        if ($menu != null) {
          foreach ($menu as $menuElement) {
            ?>
              <tr>
                <th><svg class="icon" alt=""><use xlink:href="#icon-<?php echo $menuElement->title; ?>"></use></svg> <span class="text-maj"><?php echo $menuElement->title; ?></span></th>
                <td class="text-right"><a class="pretty-link" href="<?php echo $menuElement->url; ?>"><?php echo $menuElement->url; ?></td>
              </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>

  <?php endwhile; ?>
  <?php endif; ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
