<?php // Do not delete these lines
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Ne pas t&eacute;l&eacute;charger cette page directement, merci !');
if (!empty($post->post_password)) { // if there's a password
	if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
?>

<h2><?php _e('Prot&eacute;g&eacute; par mot de passe'); ?></h2>
<p><?php _e('Entrer le mot de passe pour voir les commentaires'); ?></p>

<?php return;
	}
}

	/* This variable is for alternating comment background */

$oddcomment = 'alt';

?>

<!-- You can start editing here. -->

<section class="comment-list">

  <?php if ($comments) : ?>
    <h1 class="page-title" id="comments"><?php comments_number('Pas de commentaire', 'Un commentaire', '% commentaires' );?></h1>
    <?php foreach ($comments as $comment) : ?>
			<article class="card comment">
				<div class="author-area">
						<?php echo get_avatar( get_the_author_meta('user_email'), $size = '120'); ?>
						<div class="author-metadata">
							<div class="author-pseudo">Écrit par <a href="" >Pseudo</a></div>
							<small class="author-date">Le vendredi 19 septembre 2014 à 07:16</small>
						</div>
				</div>
				<div class="card-body">
					<?php comment_text() ?>
				</div>
			</article>

<?php /* Changes every other comment to a different class */
	if ('alt' == $oddcomment) $oddcomment = '';
	else $oddcomment = 'alt';
?>

<?php endforeach; /* end for each comment */ ?>

<?php else : // this is displayed if there are no comments so iconr ?>

<?php if ('open' == $post->comment_status) : ?>
	<!-- If comments are open, but there are no comments. -->
	<?php else : // comments are closed ?>

	<!-- If comments are closed. -->
<p class="nocomments toast toast-danger">Les commentaires sont ferms !</p>

	<?php endif; ?>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>
	<div class="card card-primary">
		<h2 id="respond" class="card-header">Laissez un commentaire</h2>

		<!-- Cas du besoin de s'enregistrer -->
		<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
		<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">connect&eacute;</a> pour laisser un commentaire.</p>

		<!-- Utilisateur enregistré -->
		<?php else : ?>
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( $user_ID ) : ?>

		<div class="toast toast-info alert-flex" role="alert">
		  <p class="mb-0"><svg class="icon icon-info" alt=""><use xlink:href="#icon-info"></use></svg> Connect&eacute; en tant que <a class="alert-link" href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.</p>
		  <p class="mb-0 align-right"><a class="alert-link" href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout"><svg class="icon icon-power-off" alt=""><path d="M27.455 16.422c0 7.561-6.167 13.728-13.728 13.728s-13.728-6.167-13.728-13.728c0-4.344 2.002-8.347 5.487-10.957 1.019-0.769 2.449-0.572 3.2 0.447 0.769 1.001 0.554 2.449-0.447 3.2-2.324 1.752-3.664 4.415-3.664 7.311 0 5.041 4.111 9.152 9.152 9.152s9.152-4.111 9.152-9.152c0-2.896-1.341-5.559-3.664-7.311-1.001-0.751-1.215-2.199-0.447-3.2 0.751-1.019 2.199-1.215 3.2-0.447 3.486 2.61 5.488 6.614 5.488 10.957zM16.016 2.695v11.44c0 1.251-1.037 2.288-2.288 2.288s-2.288-1.037-2.288-2.288v-11.44c0-1.251 1.037-2.288 2.288-2.288s2.288 1.037 2.288 2.288z"></path></svg> <span class="sr-only">Se d&eacute;connecter de ce compte</span></a></p>
		</div>

		<!-- Commentaire non-enregistré -->
		<?php else : ?>
		<div class="toast toast-info"><p>Ce site utilise Akismet pour réduire les contenus indésirables (spam, etc). <a href="https://akismet.com/privacy/">En savoir plus sur la iconçon dont les données de vos commentaires sont traitées.</a></p></div>
		<div class="input-group mb-3">
		  <input type="text" class="form-control" name="author" id="author" placeholder="Nom <?php if ($req) echo "(requis)"; ?>" aria-label="Username" value="<?php echo $comment_author; ?>" size="40" />
		</div>

		<div class="input-group mb-3">
		  <input type="text" class="form-control" name="email" id="email" placeholder="Courriel (ne sera pas publi&eacute;) <?php if ($req) echo "(requis)"; ?>" value="<?php echo $comment_author_email; ?>" size="40" />
		</div>

		<div class="input-group mb-3">
		  <input type="text" class="form-control" name="url" id="url" placeholder="Site Web" value="<?php echo $comment_author_url; ?>" size="40" />
		</div>

		<?php endif; ?>

		<div class="input-group mb-3">
		  <textarea class="form-control" name="comment" id="comment" aria-label="Contenu du commentaire" placeholder="Contenu du commentaire"></textarea>
		</div>
		<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
		<?php do_action('comment_form', $post->ID); ?>

		<p class="align-right"><button name="submit" class="btn btn-primary" type="submit" id="submit" value="Envoyer" >Envoyer</button>
		</p>


		</form>

	</div>



<?php endif; // If registration required and not logged in ?>

</section>

<?php endif; // if you delete this the sky will iconll on your head ?>
