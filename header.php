<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name'); ?><?php wp_title(' - '); ?></title>
	<meta name="description" content="<?php bloginfo('name'); echo " - "; bloginfo('description');?>" />

  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

  <?php wp_head(); ?>
  <?php include(TEMPLATEPATH . '/components/opengraph.php'); ?>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/img/favicon.png">>

	<!-- CSS -->
	<link href="<?php bloginfo('stylesheet_url'); ?>?f43dsg" rel="stylesheet"/>
</head>

<!-- Début du vrai HTML -->
<body>
  <?php include(TEMPLATEPATH . '/components/common-icons.php'); ?>
	<div class="bg-dark skip small-text">
		<a href="#skip">Accéder au contenu</a>
	</div>
  <div id="wrapper">
    <?php include(TEMPLATEPATH . '/components/header-content.php'); ?>
    <div class="container-blog">
