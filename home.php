<?php get_header(); ?>
<div class="fullwidth" id="skip">
  <?php include(TEMPLATEPATH . '/components/globalflag.php'); ?>
  <!-- Recuperation de la page de home -->
  <?php if ( !is_paged() ) {
      $page = get_page_by_path( 'home' );
      if ($page != null) {
  ?>
  <section id="description" class="p-0">
    <h1 id="desc-title" class="page-title"><?php echo get_the_title( $page ); ?></h1>
    <img class="illustration" alt="Un kobold gris et violet binoclard, avec un manteau noir plein de cables" src="<?php echo get_template_directory_uri();?>/img/zerhjyj-small.webp" height="530px" width="509px" />
    <div id="description-content">
        <div class="post-content"><?php echo $page->post_content; ?></div>
        <div class="align-centered-block"><a href="/about" class="btn btn-primary">Plus d'info</a></div>
    </div>
  </section>
  <section class="mb-2 d-none d-block-xl">
    <nav aria-label="Mes sites web">
      <ul class="no-pills f-row f-between mb-1">
      <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['link-menu']);
        foreach ($menu as $menuElement) {
          echo '<li class="m-0"><a href="' . $menuElement->url . '" class="btn btn-primary">'. $menuElement->title . '</a></li>';
        }
      ?>
      </ul>
    </nav>
  </section>
  <?php 
    }
  }
  ?>
  <div class="container-blog">
    <section>
      <div class="mb-1 d-none d-block-sm bg-light-2">
        <?php include(TEMPLATEPATH . '/components/searchform.php'); ?>
      </div>
      <?php
          echo '<h1 class="page-title">Dernier article</h1>';
          include(TEMPLATEPATH . '/components/featured.php');
      ?>
    
      <h1 class="page-title">Dernières publications</h1>
      <?php include(TEMPLATEPATH . '/components/preview-list-home.php'); ?>

      <div>
        <?php if ( !is_paged() ) {
          $page = get_page_by_path( 'buttons' );
          if ($page != null) {
            ?> <h1 class="sr-only" id="title-buttons">Boutons web</h1> <?php
            echo '<div>' . $page->post_content . '</div>';
          }
        }
      ?>
      </div>
    </section>

    <aside class="sidebar" aria-labelledby="title-sidebar">
      <h1 class="sr-only" id="title-sidebar">Menu de navigation</h1>
      <?php include(TEMPLATEPATH . '/components/sidebar/categories.php'); ?>
      <?php include(TEMPLATEPATH . '/components/sidebar/tags.php'); ?>
      <?php include(TEMPLATEPATH . '/components/sidebar/archives.php'); ?>
    </aside>
  </div>
</div>
<?php get_footer();  ?>
