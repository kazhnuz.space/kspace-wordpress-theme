<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
  <?php kspace_cat_breadcrumb(get_the_archive_title(), 'calendar'); ?>
  <?php include(TEMPLATEPATH . '/components/preview-list.php'); ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
