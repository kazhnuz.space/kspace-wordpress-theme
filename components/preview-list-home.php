<?php if(have_posts()) : ?>
  <div class="preview-grid">
    <?php 
      $max = 6;
      $i = 0;
    ?>
    <?php while(have_posts()) : the_post(); ?>
      <article class="card card-preview head-primary">
          <a href="<?php the_permalink(); ?>" class="preview-link">
            <h1 class="card-header"><?php the_title(); ?></h1>
            <div class="preview-content">
              <?php if(!has_post_thumbnail( $post->ID )) {
                ?>
                <div class="preview-exerpt" aria-hidden="true"><?php the_excerpt(); ?></div>
                <?php
              } else {
                ?>
                <div class="preview-exerpt" aria-hidden="true"><p class="p-img"><img class="preview-img" src="<?php echo get_the_post_thumbnail_url(); ?>" /></p></div>
                <?php
              }
              ?>
              <div class="preview-overlay">
                <div class="preview-metadata">
                  <div class="metadata-pills">
                    <div><?php
                    if (!has_category('chapters')) {
                      $category = get_the_category();
                      echo"<span class='badge c-info small-text'>" . $category[0]->cat_name . "</span>";
                    } else {
                      $romans = get_the_terms($post->ID, 'roman');
                      echo"<span class='badge c-info small-text'>" . $romans[0]->name . "</span>";
                    }
                    ?></div>
                    <div><time><span class="badge c-info small-text"><?php the_time('d/m/Y') ?></span></time></div>
                  </div>
                  <div class="comment-text">
                    <?php $category = get_the_category(); 
                      $category_parent_id = $category[0]->category_parent;
                      if ( get_term( $category_parent_id, 'category' )->slug != "3-gallerie" ) {
                        echo reading_time();
                      } else {
                        echo image_number();
                      }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </a>
      </article>

      <?php
        $i++;
        if ($i >= 6) {
          break;
        }
      ?>
    <?php endwhile; ?>
  </div>
<?php endif; ?>
