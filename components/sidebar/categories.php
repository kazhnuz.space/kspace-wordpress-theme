<?php
  $parent_categories = get_categories( array(
    'orderby' => 'slug',
    'order'   => 'ASC',
    'parent'  => 0
  ) );

  foreach( $parent_categories as $parent_category ) {
?>
    <section class="card head-primary d-none d-flex-sm">
      <h2 class="card-header"><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg> <?php echo $parent_category->name ?></h2>
      <div class="menu fg-dark">
        <ul>
          <?php
            $categories = get_categories( array(
              'orderby' => 'slug',
              'order'   => 'ASC',
              'parent'  => $parent_category->term_id
            ) );

            foreach( $categories as $category ) {
              if ($category->slug != "chapters") {
                echo '<li><a class="menu-element" href="' . get_category_link($category->term_id) . '">' . $category->name . '<span class="badge bg-secondary">'. $category->count . '</span></a></li>';
                }
            }
          ?>
        </ul>
      </div>
    </section>
<?php      
  }
?>
