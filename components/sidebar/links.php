<?php
$listmenu = get_nav_menu_locations();
$menu = wp_get_nav_menu_items($listmenu['link-menu']);
if ($menu != null) {
?>
<section class="card head-primary">
  <h2 class="card-header">
    Liens
  </h2>
  <div class="menu fg-dark">
    <ul>
      <?php
        $menu = wp_get_nav_menu_items( 'link-menu');
          foreach ($menu as $menuElement) {
            echo '<li><a href="' . $menuElement->url . '" class="menu-element">'. $menuElement->title . '</a></li>';
          }
        ?>
    </ul>
  </div>
</section>
<?php
}
?>
