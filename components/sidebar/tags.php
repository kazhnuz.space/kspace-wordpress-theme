<?php

$args = array(
        'smallest' => 8, 'largest' => 22, 'unit' => 'pt', 'number' => 999,
        'format' => 'array', 'orderby' => 'name', 'order' => 'ASC',
        'exclude' => '', 'include' => '');

$tags = get_tags($args);

?>

<section class="card head-primary">
  <h2 class="card-header"><svg class="icon icon-tags" alt=""><use xlink:href="#icon-tags"></use></svg> Tags</h2>
  <div class="card-body">
    <ul class="tag-list">
      <?php foreach ($tags as $tag) {?>
        <li><a href="<?php bloginfo('url'); ?>/tag/<?php echo $tag->slug ?>"><?php echo $tag->name ?></a></li>
      <?php } ?>
    </ul>
  </div>
</section>
