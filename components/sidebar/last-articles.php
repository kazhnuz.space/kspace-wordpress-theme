<section class="card head-primary">
  <h2 class="card-header"></i> Publications</h2>
  <div class="menu fg-dark">
    <ul class="trim-that">
      <?php
        wp_get_archives( array(
          'type'  => 'postbypost',
          'echo'  => 1,
          'order' => 'ASC',
          'limit' => 6
        ) );
        ?>
    </ul>
  </div>
</section>
