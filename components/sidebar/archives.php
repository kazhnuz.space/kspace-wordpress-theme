<section class="card head-primary">
  <h2 class="card-header"><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg> Archives</h2>
  <div class="card-body">
    <ul class="tag-list">
      <?php wp_get_archives('type=yearly&format=html&show_post_count=0'); ?>
    </ul>
  </div>
</section>
