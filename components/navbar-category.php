<?php
  $parent_categories = get_categories( array(
    'orderby' => 'slug',
    'order'   => 'ASC',
    'parent'  => 0
  ) );

  foreach( $parent_categories as $parent_category ) {
?>
  <li>
    <a class="menu-item submenu" href="#">
      <?php echo $parent_category->name ?> <svg class="icon icon-caret-down" alt=""><use xlink:href="#icon-caret-down"></use></svg></a>

      <ul class="bg-light menu fg-dark">
        <?php
          $categories = get_categories( array(
            'orderby' => 'slug',
            'order'   => 'ASC',
            'parent'  => $parent_category->term_id
          ) );

          foreach( $categories as $category ) {
            if ($category->slug != "chapters") {
              echo "<!-- " . get_category_link($category->term_id) . " -->";
              echo '<li><a class="menu-element" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
              }
          }
        ?>
      </ul>
      
  </li>      
<?php      
  }
?>
