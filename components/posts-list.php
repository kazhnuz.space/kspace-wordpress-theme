<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
  <div class="card head-primary" id="post-<?php the_ID(); ?>">
    <h1 class="card-header"><?php the_title(); ?></h1>
    <div class="flex-that mb">
        <div class="article-category">
          <?php $category = get_the_category();
            echo"<a href= '" . esc_url( get_category_link( $category[0]->term_id ) ) . "' class='btn btn-small btn-info'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg> " . $category[0]->cat_name . "</a>"; ?>
        </div>
        <time itemprop="datePublished"><em><?php the_time('j F Y') ?></em></time>
    </div>
    <?php the_excerpt(); ?>
    <p class="align-center"> <a href="<?php the_permalink(); ?>" class="btn btn-readmore" title="<?php the_title();?>">Lire l'article</a> </p>
  </div>
  <?php endwhile; ?>
  <div class="navigation">
    <?php the_posts_pagination(); ?>
  </div>
<?php endif; ?>
