<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

<?php include(TEMPLATEPATH . '/components/article/flags.php'); ?>

<article class="article container-article" id="post-<?php the_ID(); ?>">
  <?php if ($haveTitle) { ?>
    <h1 class="page-title"><?php the_title(); ?></h1>

    <?php $category = get_the_category(); 
    $category_parent_id = $category[0]->category_parent;
    if ( get_term( $category_parent_id, 'category' )->slug != "3-gallerie" ) {?>
      <p class="align-right pr-half"><span class="btn btn-small c-secondary"> <?php echo reading_time(); ?> </span></p>
    <?php }?>
  <?php } else { ?>
    <h1 class="sr-only"><?php the_title(); ?></h1>
  <?php } ?>

  <div id="myarticle" class="article-entry mb-1">
    <?php the_content(); ?>
  </div>
</article>

<section class="card card-noheader">
  <div class="author-area">
    <?php echo get_avatar( get_the_author_meta('user_email'), $size = '120'); ?>
    <div class="author-metadata">
      <div class="author-pseudo">Écrit par <a href="https://kazhnuz.space"><?php the_author() ?></a></div>
      <small class="author-date">Le <?php the_time('l j F Y à H:i') ?></small>
    </div>
  </div>
  <div class="card-body">
    <?php include(TEMPLATEPATH . '/components/article/description.php'); ?>

    <div class="article-category">
      <?php include(TEMPLATEPATH . '/components/article/terms.php'); ?>
    </div>
  </div>
</section>

<?php endwhile; ?>
<?php endif; ?>
