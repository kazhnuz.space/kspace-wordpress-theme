<nav id="mobile-sidebar" class="bg-dark fg-light sidebar menu hidden">
  <h1 class="align-center text-light">Menu du site</h1>
  <div class="mb-1">
    <?php include(TEMPLATEPATH . '/components/searchform.php'); ?>
  </div>
  <h2>Menu principal</h2>
  <ul>
    <li>
      <a href="<?php echo site_url(); ?>" class="menu-item">
        <span><svg class="icon icon-home" alt=""><use xlink:href="#icon-home"></use></svg> Accueil</span>
      </a>
    </li>
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar']);
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>

  <?php
  $parent_categories = get_categories( array(
    'orderby' => 'slug',
    'order'   => 'ASC',
    'parent'  => 0
  ) );

  foreach( $parent_categories as $parent_category ) {
?>
  <h2><?php echo $parent_category->name ?></h2>
  <ul>
    <?php
      $categories = get_categories( array(
        'orderby' => 'slug',
        'order'   => 'ASC',
        'parent'  => $parent_category->term_id
      ) );

      foreach( $categories as $category ) {
        if ($category->slug != "chapters") {
          echo "<!-- " . get_category_link($category->term_id) . " -->";
          echo '<li><a class="menu-element" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
          }
      }
    ?>
  </ul>
      
<?php      
  }
?>
  <h2>Pages</h2>
  <ul>
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar-2']);
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>
</nav>

<button id="mobile-button" class="menu-button"><svg class="icon icon-bars" alt=""><use xlink:href="#icon-bars"></use></svg> <span class="sr-only">Afficher le menu</span></button>