<?php $flags = get_the_terms($post->ID, 'flag');
if ($flags) {
  foreach( $flags as $flag ) {
    $term_meta = get_option( "taxonomy_term_$flag->term_id" );
    echo "<div class='toast bg-" . $term_meta['niveau'] . "'>";
    echo $flag->description;
    echo "</div>";
  }
} ?>
