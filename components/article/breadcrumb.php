<div class="flex-that d-none d-flex-sm pl-half pr-half">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class='breadcrumb-item'><a href='<?php echo site_url(); ?>'>kazhnuz.space</a></li><?php
      if (has_category('chapters')) {
        $romans = get_the_terms($post->ID, 'roman');
        foreach( $romans as $roman ) {
          echo "<li class='breadcrumb-item'><a href='" . esc_url( get_category_link( $roman->term_id ) ) . "'> " . $roman->name . "</a></li>";
        }
      } else {
        $categories = get_the_category();
        foreach( $categories as $category ) {
          echo "<li class='breadcrumb-item'><a href='" . esc_url( get_category_link( $category->term_id ) ) . "'> " . $category->cat_name . "</a></li>";
        }

      }
       ?><li class="breadcrumb-item" aria-current="page"><span class="active"> <?php echo the_title(); ?></span></li>
    </ol>
  </nav>
</div>
