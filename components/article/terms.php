<h3 class="sr-only">Tags et catégories</h3>
<?php
  if (has_category('chapters')) {
    echo "<ul class='nolist' aria-labelledby='title-article-taxo-romans'>";
    echo '<h4 class="sr-only" id="title-article-taxo-romans">Romans</h4>';
    $romans = get_the_terms($post->ID, 'roman');
    foreach( $romans as $roman ) {
      echo "<li><a href= '" . esc_url( get_category_link( $roman->term_id ) ) . "' class='btn btn-small c-primary mr-half'><i class='icon icon-book'></i>&nbsp;" . $roman->name . "</a></li>";
    }
    echo "</ul>";
  } else {
    $categories = get_the_category();
    echo "<ul class='nolist' aria-labelledby='title-article-taxo-categories'>";
    echo '<h4 class="sr-only" id="title-article-taxo-categories">Catégories</h4>';
    foreach( $categories as $category ) {
      echo "<li><a href= '" . esc_url( get_category_link( $category->term_id ) ) . "' class='btn btn-small c-primary mr-half'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg>&nbsp;" . $category->cat_name . "</a></li>";
    }
    echo "</ul>";
  }
  $tags = get_the_tags();
  if ($tags) {
    echo '<h4 class="sr-only" id="title-article-taxo-tags">Tags</h4>';
    echo "<ul class='nolist' aria-labelledby='title-article-taxo-tags'>";
    foreach( $tags as $tag ) {
      echo "<li><a href= '" . esc_url( get_tag_link( $tag->term_id ) ) . "' class='btn btn-small c-secondary mr-half'><i class='icon icon-tag'></i>&nbsp;" . $tag->name . "</a></li>";
    }
    echo "</ul>";
  }
?>
