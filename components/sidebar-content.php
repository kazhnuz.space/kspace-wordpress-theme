<div class="mb-1 d-none d-block-sm bg-light-2">
  <?php include(TEMPLATEPATH . '/components/searchform.php'); ?>
</div>

<?php include(TEMPLATEPATH . '/components/sidebar/last-articles.php'); ?>
<?php include(TEMPLATEPATH . '/components/sidebar/categories.php'); ?>
<?php include(TEMPLATEPATH . '/components/sidebar/tags.php'); ?>
<?php include(TEMPLATEPATH . '/components/sidebar/archives.php'); ?>
