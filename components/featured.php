<div id="featured-articles">
  <?php
  query_posts(array(
    'post_type' => 'post',
    'tax_query' => array(
        array(
            'taxonomy' => 'niveau',
            'terms' => 'featured',
            'field' => 'slug',
        )
    ),
    'orderby' => 'date',
    'post_count' => '1',
    'order' => 'DESC',
    'posts_per_page' => '1'
  ));

  if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

  <article class="card head-primary" id="post-<?php the_ID(); ?>">
    <h1 class="card-header"><?php the_title(); ?></h1>
    <div class="flex-that mb">
        <div class="article-category">
          <?php $category = get_the_category();
            echo"<a href= '" . esc_url( get_category_link( $category[0]->term_id ) ) . "' class='btn btn-small c-info'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg> " . $category[0]->cat_name . "</a>"; ?>
        </div>
        <time itemprop="datePublished"><em><?php the_time('j F Y') ?></em></time>
    </div>
    <?php the_excerpt(); ?>
    <p class="align-center"> <a href="<?php the_permalink(); ?>" class="btn btn-link" title="<?php the_title();?>">Lire l'article</a> </p>
  </article>

  <?php endwhile; ?> <?php wp_reset_query(); /*4*/ ?>
</div>
