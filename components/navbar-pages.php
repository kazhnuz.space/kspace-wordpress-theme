<nav class="container menu toolbar flex-that fg-light d-none d-flex-sm">
  <h2 class="sr-only">Menu des pages</h2>
  <ul>
    <li>
      <a href="<?php echo site_url(); ?>" >
        <svg class="icon icon-topbar" alt=""><use xlink:href="#icon-home"></use></svg><span class="sr-only">Accueil</span>
      </a>
    </li>
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar']);
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" >'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>

  <ul class="f-end">
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar-2']);
        foreach ($menu as $menuElement) {
          echo '<li class="d-none d-block-sm"><a href="' . $menuElement->url . '">'. $menuElement->title . '</a></li>';
        }
      ?>
    <li>
        <a class="menu-item submenu" href="#">Mes sites <svg class="icon icon-caret-down" alt=""><use xlink:href="#icon-caret-down"></use></svg></a>
        <ul class="menu fg-dark">
        <?php
          $listmenu = get_nav_menu_locations();
          $menu = wp_get_nav_menu_items($listmenu['link-menu']);
          foreach ($menu as $menuElement) {
            echo '<li><a href="' . $menuElement->url . '">'. $menuElement->title . '</a></li>';
          }
        ?>
        </ul>
    </li>
    <li><a href="<?php bloginfo('rss2_url'); ?>" ><svg class="icon icon-topbar" alt=""><use xlink:href="#icon-rss"></use></svg><span class="sr-only">Flux RSS du site</span></a></li>
  </ul>
</nav>

