<?php /* Template Name: page-about */ ?>
<?php get_header(); ?> <!-- ouvrir header,php -->
<main>
  <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

  <article class="article-content container-article mb-1" id="post-<?php the_ID(); ?>">
  <h1 class="page-title"><?php the_title(); ?></h1>

    <div class="article-body">
      <?php the_content(); ?>
    </div>
  </article>

  <?php endwhile; ?>
  <?php endif; ?>

  <div class="card head-primary" id="social">
    <h2 class="card-header">Me retrouver sur les réseaux sociaux</h2>
    <table class="head-info">
      <thead>
        <tr>
          <th class="text-info">Réseau social</th>
          <th class="text-right text-info">Lien</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['social']);
        if ($menu != null) {
          foreach ($menu as $menuElement) {
            ?>
              <tr>
                <th><svg class="icon" alt=""><use xlink:href="#icon-<?php echo $menuElement->title; ?>"></use></svg> <span class="text-maj"><?php echo $menuElement->title; ?></span></th>
                <td class="text-right"><a class="pretty-link" href="<?php echo $menuElement->url; ?>"><?php echo $menuElement->url; ?></td>
              </tr>
            <?php
          }
        }
        ?>
        <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['social-plus']);
        if ($menu != null) {
          foreach ($menu as $menuElement) {
            ?>
              <tr>
                <th><svg class="icon" alt=""><use xlink:href="#icon-<?php echo $menuElement->title; ?>"></use></svg> <span class="text-maj"><?php echo $menuElement->title; ?></span></th>
                <td class="text-right"><a class="pretty-link" href="<?php echo $menuElement->url; ?>"><?php echo $menuElement->url; ?></td>
              </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>

</main>
<aside class="sidebar">
  <div class="card head-info">
    <?php $desc = explode(";", get_the_author_meta( 'infodata' )) ?>
    <div class="card-header"><svg class="icon icon-home" alt=""><use xlink:href="#icon-home"></use></svg> <?php
      if (!empty($desc[1])) {
        echo $desc[1];
      } else {
        the_author();
      }
    ?></div>
    <p class="mwarea"><?php echo get_avatar( get_the_author_meta('user_email'), $size = '500'); ?></p>
    <table style="width:100%" class="head-primary">
        <tr>
          <th>Pseudo</th>
          <td><?php the_author() ?></td>
        </tr>
        <?php $user_description = $desc[0];
        $array_tr = explode ( ",", $user_description);
        foreach ($array_tr as $tr) {
          echo "<tr>";
          $array_td = explode(':', $tr);
          echo "<th>" . $array_td[0] . "</th>";
          echo "<td>" . $array_td[1] . "</td>";
          echo "</tr>";
        }
        ?>
      </table>
  </div>

  <?php include(TEMPLATEPATH . '/components/sidebar-content.php'); ?>
</aside>
<?php get_footer(); ?>
