<?php get_header(); ?>
<main id="skip">
  <h1 class="page-title">Page non trouvée</h1>
  <p>La page que vous cherchez n'a pas été trouvée. Pour la peine, voilà un petit Kazhnuz pour vous remonter le moral. Vous pouvez le faire !</p>
  <p class="align-center"><img src="<?php echo get_template_directory_uri();?>/img/cutenuz.png" /></p>
  <p>Merci à <a href="https://withelias.kobold.cafe">Withelias</a> pour le dessin !</p>
</main>
<?php get_sidebar(); ?>
<?php get_footer();  ?>
