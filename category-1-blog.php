<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
  <?php $category = get_category( get_query_var( 'cat' ) );
    kspace_cat_breadcrumb_with_rss(single_cat_title('', false), 'folder-open', 'la catégorie',  get_category_link( $category->cat_ID ) . '/feed' );
  ?>

  <div class="well">
    <?php the_archive_description() ?>
  </div>

  <?php include(TEMPLATEPATH . '/components/posts-list.php'); ?>
</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
