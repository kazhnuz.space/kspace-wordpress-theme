<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
    <?php $tag = get_category( get_query_var( 'tag' ) );
      kspace_cat_breadcrumb_with_rss(single_cat_title('', false), 'tag', 'RSS du tag',  get_tag_link( $tag->cat_ID ) . '/feed' );
    ?>

    <div class="well">
      <?php echo tag_description();?>
      <?php $infourl = get_term_meta(get_queried_object_id(), 'plus-dinfo', true);
      if ($infourl != null) {
        echo '<p class="align-center"><a href="' . $infourl . '" class="btn btn-readmore">Plus d\'info</a></p>';
      }
      ?>
    </div>

    <?php include(TEMPLATEPATH . '/components/preview-list.php'); ?>
</main>
<!-- Sidebar custom pour contenir la description -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
