document.getElementById('mobile-button').addEventListener('click', function () {
  const sidebar = document.getElementById('mobile-sidebar');
  if (sidebar.classList.contains('hidden')) {
    sidebar.classList.remove('hidden');
    sidebar.classList.add('shown');
  } else {
    sidebar.classList.remove('shown');
    sidebar.classList.add('hidden');
  }
});