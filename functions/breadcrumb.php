<?php
/////////////////////////////////////////////////////////////////////////////
// Breadcrumb supports
// Functions to show a breadcrumb
// @author Kazhnuz

function kspace_cat_breadcrumb_nav($categoryName, $icon) {
?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">kazhnuz.space</a>
            </li><li class="breadcrumb-item" aria-current="page">
                <span class="active"><svg class="icon icon-folder-open" alt="" viewBox="0 0 34 32"><path d="M33.586 17.423c0 0.429-0.268 0.858-0.554 1.18l-6.006 7.078c-1.037 1.215-3.146 2.181-4.719 2.181h-19.448c-0.643 0-1.555-0.197-1.555-1.001 0-0.429 0.268-0.858 0.554-1.18l6.006-7.078c1.037-1.215 3.146-2.181 4.719-2.181h19.448c0.643 0 1.555 0.197 1.555 1.001zM27.455 11.274v2.86h-14.872c-2.234 0-5.005 1.269-6.453 2.985l-6.113 7.186c0-0.143-0.018-0.304-0.018-0.447v-17.16c0-2.199 1.805-4.004 4.004-4.004h5.72c2.199 0 4.004 1.805 4.004 4.004v0.572h9.724c2.199 0 4.004 1.805 4.004 4.004z"></path></svg> <?php echo $categoryName; ?></span>
            </li>
        </ol>
    </nav>
<?php
}

function kspace_cat_breadcrumb($categoryName, $icon) {
?>
    <div class="flex-that d-none d-flex-sm pr-half pl-half">
        <?php kspace_cat_breadcrumb_nav($categoryName, $icon); ?>
    </div>
<?php
}

function kspace_cat_breadcrumb_with_rss($categoryName, $icon, $categoryType, $rssLink) {
?>
    <div class="flex-that d-none d-flex-sm pr-half pl-half">
        <?php kspace_cat_breadcrumb_nav($categoryName, $icon); ?>
        <div class="rss">
            <a href="<?php echo $rssLink; ?>" class="btn btn-warning d-block m-0" /><svg class="icon icon-rss" alt=""><use xlink:href="#icon-rss"></use></svg><span class="sr-only">Flux RSS de <?php echo $categoryType; ?></span></a>
        </div>
    </div>
<?php
}


?>