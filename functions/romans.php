<?php
/////////////////////////////////////////////////////////////////////////////
// Romans
// Permet de catégoriser des chapitres ensemble, via une catégorie spécifiques.
//

register_taxonomy('roman', 'post',
  array(
    'label' => 'Roman',
    'labels' => array(
      'name' => 'Romans',
      'singular_name' => 'Roman',
      'all_items' => 'Tous les romans',
      'edit_item' => 'Éditer le roman',
      'view_item' => 'Voir le roman',
      'update_item' => 'Mettre à jour le roman',
      'add_new_item' => 'Ajouter un roman',
      'new_item_name' => 'Nouveau roman',
      'search_items' => 'Rechercher parmi les romans',
      'popular_items' => 'Romans les plus utilisés'),
    'rewrite'           =>  array('slug' => 'roman'),
    'hierarchical'      =>  false,
    'public'            =>  true,
    'hierarchical'      =>  true,
    'show_in_nav_menus' =>  true,
    'has_archive'       =>  true,
  )
);

 register_taxonomy_for_object_type( 'roman', 'post' );

 // 1.2 - Ajout/Modif du niveau du flag
 function romans_taxonomy_custom_fields($tag) {
    // Check for existing taxonomy meta for the term you're editing
     $t_id = $tag->term_id; // Get the ID of the term you're editing
     $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
 ?>

 <tr class="form-field">
     <th scope="row" valign="top">
         <label for="niveau"><?php _e('Couverture du roman'); ?></label>
     </th>
     <td>
         <input type="text" name="term_meta[cover]" id="term_meta[cover]" size="25" style="width:60%;" value="<?php echo $term_meta['cover'] ? $term_meta['cover'] : ''; ?>"><br />
         <span class="description"><?php _e('La couverture du roman, sous format URL'); ?></span>
     </td>
 </tr>

 <?php
 }

 function romans_taxonomy_custom_fields_add( $taxonomy ) {
    // Check for existing taxonomy meta for the term you're editing
     $t_id = $tag->term_id; // Get the ID of the term you're editing
     $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
 ?>

 <div class="form-field">
   <label for="term_meta[cover]"><?php _e('Couverture du roman'); ?></label>
   <input type="text" name="term_meta[cover]" id="term_meta[cover]" />
   <p><?php _e('La couverture du roman, sous format URL)'); ?>.</p>
 </div>

 <?php
 }

 // Ajout des fields
 add_action( 'roman_edit_form_fields', 'romans_taxonomy_custom_fields', 10, 2 );
 add_action( 'roman_add_form_fields', 'romans_taxonomy_custom_fields_add' );
 add_action( 'created_roman', 'save_taxonomy_custom_fields' );
 add_action( 'edited_roman', 'save_taxonomy_custom_fields', 10, 2 );

?>