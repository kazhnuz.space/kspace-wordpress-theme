<?php
/////////////////////////////////////////////////////////////////////////////
// Tags - More Info
// Permet d'afficher un lien vers plus d'informations.
// Le liens est gardé comme un champ supplémentaire dans les tags.
//


add_action( 'post_tag_add_form_fields', 'kspace_add_term_fields_moreinfo' );

function kspace_add_term_fields_moreinfo( $taxonomy ) {
	echo '<div class="form-field">
	<label for="plus-dinfo">Plus d\'Info</label>
	<input type="text" name="plus-dinfo" id="plus-dinfo" />
	<p>Une URL permettant d\'obtenir plus d\'information sur le tag.</p>
	</div>';
}

add_action( 'post_tag_edit_form_fields', 'kspace_edit_term_fields_moreinfo', 10, 2 );

function kspace_edit_term_fields_moreinfo( $term, $taxonomy ) {

	$value = get_term_meta( $term->term_id, 'plus-dinfo', true );

	echo '<tr class="form-field">
	<th>
		<label for="plus-dinfo">Plus d\'Info</label>
	</th>
	<td>
		<input name="plus-dinfo" id="plus-dinfo" type="text" value="' . esc_attr( $value ) .'" />
		<p class="description">Une URL permettant d\'obtenir plus d\'information sur le tag.</p>
	</td>
	</tr>';
}

add_action( 'created_post_tag', 'kspace_save_term_fields_moreinfo' );
add_action( 'edited_post_tag', 'kspace_save_term_fields_moreinfo' );

function kspace_save_term_fields_moreinfo( $term_id ) {

	update_term_meta(
		$term_id,
		'plus-dinfo',
		sanitize_text_field( $_POST[ 'plus-dinfo' ] )
	);

}


?>