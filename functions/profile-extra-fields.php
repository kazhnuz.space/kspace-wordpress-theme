<?php
/////////////////////////////////////////////////////////////////////////////
// Info supplémentaire sur profil utilisateur
// Permet d'afficher sous forme de chaine de caractères des infos supplémentaires qui seront
// transformé en un tableau.

add_action( 'show_user_profile', 'kspace_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'kspace_show_extra_profile_fields' );

function kspace_show_extra_profile_fields( $user ) {
	$year = get_the_author_meta( 'infodata', $user->ID );
	?>
	<h3><?php esc_html_e( 'More Info', 'kspace' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="infodata"><?php esc_html_e( 'infodata', 'kspace' ); ?></label></th>
			<td>
				<input type="input"
			       id="infodata"
			       name="infodata"
			       value="<?php echo esc_attr( $year ); ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
	</table>
	<?php
}

add_action( 'personal_options_update', 'kspace_update_profile_fields' );
add_action( 'edit_user_profile_update', 'kspace_update_profile_fields' );

function kspace_update_profile_fields( $user_id ) {
	if ( ! current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}

	update_user_meta( $user_id, 'infodata', $_POST['infodata'] );
}

?>