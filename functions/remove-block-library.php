<?php
//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css(){
 wp_dequeue_style( 'wp-block-library' );
 wp_dequeue_style( 'wp-block-library-theme' );
 wp_dequeue_style( 'wp-components-theme' );
 wp_dequeue_style( 'wp-components' );
 wp_dequeue_style( 'components' );
 wp_dequeue_style( 'classic-theme-styles' );
 wp_dequeue_style( 'wc-blocks-style' ); // Remove WooCommerce block CSS
 wp_deregister_style( 'dashicons' );
 add_filter( 'show_admin_bar', '__return_false' );
} 
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
?>