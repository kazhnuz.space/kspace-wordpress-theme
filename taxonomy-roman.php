<?php get_header(); ?> <!-- ouvrir header,php -->
<main>
    <?php kspace_cat_breadcrumb(single_cat_title('', false), 'book'); ?>

    <img class="cover" src="<?php
    $term_meta = get_option( "taxonomy_term_$tag_id" );
    echo $term_meta['cover'];
     ?>">
		<div class="roman">
	    <div class="card">
				<h1><?php echo single_cat_title(); ?></h1>
				<div class="card-body">
					<?php echo tag_description();?>
				</div>
	    </div>

      <?php
        $args = array(
        'tax_query' => array(
            array(
            'taxonomy' => 'roman',
            'field' => 'term_id',
            'terms' => $tag_id // you need to know the term_id of your term "example 1"
            ),
          ),
        'posts_per_page' => -1,
        'order' => 'ASC',
        );
        $query = new WP_Query( $args );
      ?>
      <svg class="icon" alt=""></svg>
			<div class="card head-primary">
			  <div class="card-header"><<svg class="icon" alt=""><path d="M4.576 23.858v3.432c0 0.304-0.268 0.572-0.572 0.572h-3.432c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h3.432c0.304 0 0.572 0.268 0.572 0.572zM4.576 16.994v3.432c0 0.304-0.268 0.572-0.572 0.572h-3.432c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h3.432c0.304 0 0.572 0.268 0.572 0.572zM4.576 10.13v3.432c0 0.304-0.268 0.572-0.572 0.572h-3.432c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h3.432c0.304 0 0.572 0.268 0.572 0.572zM32.031 23.858v3.432c0 0.304-0.268 0.572-0.572 0.572h-24.023c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h24.023c0.304 0 0.572 0.268 0.572 0.572zM4.576 3.267v3.432c0 0.304-0.268 0.572-0.572 0.572h-3.432c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h3.432c0.304 0 0.572 0.268 0.572 0.572zM32.031 16.994v3.432c0 0.304-0.268 0.572-0.572 0.572h-24.023c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h24.023c0.304 0 0.572 0.268 0.572 0.572zM32.031 10.13v3.432c0 0.304-0.268 0.572-0.572 0.572h-24.023c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h24.023c0.304 0 0.572 0.268 0.572 0.572zM32.031 3.267v3.432c0 0.304-0.268 0.572-0.572 0.572h-24.023c-0.304 0-0.572-0.268-0.572-0.572v-3.432c0-0.304 0.268-0.572 0.572-0.572h24.023c0.304 0 0.572 0.268 0.572 0.572z"></path></svg> Chapitres</div>
			  <div class="card-menu">
          <?php if($query->have_posts()) : ?>
            <?php $i = 1; ?>
              <?php while($query->have_posts()) : $query->the_post(); ?>
                 <a href="<?php the_permalink(); ?>" class="menu-element">
                   <span class="text-dark">
                     <strong>Chapitre <?php echo $i++; ?>.</strong>
                     <?php the_title(); ?>
                   </span>
                   <span>
                     <em class="text-dark"><?php the_time('d/m/Y') ?></em>
                   </span>
                 </a>
              <?php endwhile; ?>
          <?php endif; ?>
			  </div>
			</div>

		</div>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
