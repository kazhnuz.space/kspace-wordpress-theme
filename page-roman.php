<?php /* Template Name: page-romans */ ?>
<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
  <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
  <?php kspace_cat_breadcrumb('Romans', 'book'); ?>

  <div class="well">
    <?php the_content(); ?>
  </div>

  <?php endwhile; ?>
  <?php endif; ?>
  <div class="preview-grid">
  <?php $terms = get_terms(array('taxonomy'   => 'roman','hide_empty' => false));?>

  <?php if ( ! empty( $terms ) && is_array( $terms ) ) : ?>
      <?php foreach ( $terms as $term ) : ?>
        <article class="card card-preview card-primary">
          <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="preview-link">
            <div class="preview-item">
              <?php
                $tag_id = $term->term_id;
                $term_meta = get_option( "taxonomy_term_$tag_id" );
                $coverimg = $term_meta['cover'];
                echo '<div class="preview-content" style="background-image:url(\'' . $coverimg . '\');"></div>';
               ?>
              <div class="preview-overlay">
                <h1 class="card-header"><?php echo $term->name; ?></h1>
                <div class="preview-metadata">
                  <div class="metadata-pills">
                  </div>
                  <div class="comment-text"><?php echo $term->count; ?> Chapitres</div>
                </div>
              </div>
            </div>
          </a>
      </article>
    <?php endforeach; ?>
  <?php endif; ?>
  </div>
</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
